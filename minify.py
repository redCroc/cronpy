import os.path
import sys
import re

import cssmin
import jsmin


def calc_spread(match):
    for char in '+-/*':
        if char in match.group(0):
            return match.group(0).replace(char, ' {} '.format(char))
    return match.group(0)


def main():
    root = sys.argv[1]
    for filename in os.listdir(os.path.join(root, 'js/')):
        print('[+] minify {}...'.format(filename))
        with open(os.path.join(root, 'js/', filename), 'r') as file:
            with open(os.path.join(root, 'jsmin/', filename), 'w') as min_file:
                min_file.write(jsmin.jsmin(file.read()))

    for filename in os.listdir(os.path.join(root, 'css/')):
        print('[+] minify {}'.format(filename))
        with open(os.path.join(root, 'css/', filename), 'r') as file:
            with open(os.path.join(root, 'cssmin/', filename), 'w') as min_file:
                min_css = cssmin.cssmin(file.read())
                min_css = re.sub(r'calc\([^)]+\)', calc_spread, min_css)
                min_file.write(min_css)


if __name__ == '__main__':
    main()
