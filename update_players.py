from traceback import format_exc
import os.path

from providers.controller import ProviderController
from core.webutils import WebUtils
from lib.country import Country
from lib.player import Player
from lib.team import Team
import setting


class UpdatePlayers(WebUtils):

    def __init__(self):
        super().__init__(module_='update_players')
        teams = list(Team.get_teams(origin=self.module, id_team=self.args.id_team, db=self.mysqldb))
        self.images = list()
        self.countries = list(Country.get_countries(self.mysqldb))
        self.nb_tasks = len(teams)
        self.start()
        self.run(teams, self.update_players)
        self.run(self.images, self.update_image)
        self.end()

    def update_players(self, team, data):
        try:
            provider = ProviderController.get_provider(team.url)
            for player in provider.get_team_players(data, team, self.countries):
                player.store(self.mysqldb)
                player.get_image_details(self.mysqldb)
                if player.image.last_save < player.image.last_modified and 'default.jpg' not in player.image.url:
                    self.images.append(player.image)

        except BaseException as e:
            team.error = format_exc()
            self.logger.error('[-] team {}: {} - {}\n{}'.format(team.id, type(e), e, team.error))
            self.errors += 1
        else:
            team.error = None
            self.logger.info('[+] team {}: OK'.format(team.id))
            self.tasks_done += 1
            self.quantity += 1
        finally:
            team.store_error(self.mysqldb)

    def update_image(self, image, data):
        player = Player(full_name=image.full_name, birth_date=image.birth_date)
        player.image = image
        try:
            with open(os.path.join(setting.IMAGES_FOLDER, 'player', str(image.path)), 'wb') as fp:
                fp.write(data)
            player.store_image(self.mysqldb)

        except BaseException as e:
            player.error = format_exc()
            self.logger.error('[-] player {}: {} - {}\n{}'.format(player.full_name, type(e), e, player.error))
            self.errors += 1
        else:
            player.error = None
            self.logger.info('[+] player {}: OK'.format(player.full_name))
            self.quantity += 1
        finally:
            player.store_error(self.mysqldb)


if __name__ == '__main__':
    UpdatePlayers()
