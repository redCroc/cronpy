from traceback import format_exc

from providers.controller import ProviderController
from lib.league import League
from core.webutils import WebUtils


class UpdateRankings(WebUtils):

    def __init__(self):
        super().__init__(module_='update_rankings')
        leagues = list(League.get_leagues(db=self.mysqldb, origin=self.module, id_league=self.args.id_league))
        self.nb_tasks = len(leagues)
        self.start()
        self.run(leagues, self.update_league_ranking)
        self.end()

    def update_league_ranking(self, league, data):
        try:
            provider = ProviderController.get_provider(league.url)
            league.teams = league.get_teams(self.mysqldb)
            groups = provider.get_league_ranking(league, data)

            if provider.__name__ == 'Eurosport' and groups:
                self.run(groups, self.update_group_ranking)
            else:
                for team in league.teams:
                    team.set_stats(self.mysqldb)
                if groups:
                    league.store_groups(groups, self.mysqldb)

        except BaseException as e:
            league.error = format_exc()
            self.logger.error('[-] league {}: {} - {}\n{}'.format(league.id, type(e), e, league.error))
            self.errors += 1
        else:
            league.error = None
            self.logger.info('[+] league {}: OK'.format(league.id))
            self.tasks_done += 1
            self.quantity += 1
        finally:
            league.store_error(self.mysqldb)

    def update_group_ranking(self, group, data):
        try:
            provider = ProviderController.get_provider(group.url)
            provider.get_group_ranking(group, data)
            for team in group.league.teams:
                team.set_stats(self.mysqldb)

        except BaseException as e:
            group.league.error = format_exc()
            self.logger.error('[-] league {} group {}: {} - {}\n{}'.format(
                group.league.id, group.name, type(e), e, group.league.error
            ))
        else:
            group.league.error = None
            self.logger.info('[+] league {} group {}: OK'.format(group.league.id, group.name))
        finally:
            group.league.store_error(self.mysqldb)


if __name__ == '__main__':
    UpdateRankings()
