from traceback import format_exc

from providers.controller import ProviderController
from lib.tvchannel import TvChannel
from lib.league import League
from lib.match import Match
from core.webutils import WebUtils


class UpdateTVSchedule(WebUtils):
    def __init__(self):
        super().__init__(module_='update_tvschedule')
        leagues = list(League.get_leagues(db=self.mysqldb, origin=self.module, id_league=self.args.id_league))
        self.tv_channels = [tvc for tvc in TvChannel.get_tvchannels(db=self.mysqldb) if tvc.names is not None]
        self.nb_tasks = len(leagues)
        self.start()
        self.run(leagues, self.update_tvschedule)
        self.end()

    def update_tvschedule(self, league, data):
        try:
            league.matches = list(Match.get_matches(db=self.mysqldb, origin=self.module, id_league=league.id))
            provider = ProviderController.get_provider(league.url)
            provider.get_tvschedule(league, self.tv_channels, data)
            for match in league.matches:
                if match.tv_channels:
                    match.store_tvchannels(db=self.mysqldb)
                    self.quantity += 1

        except BaseException as e:
            self.logger.error('[-] scheduler {}: {} - {}\n{}'.format(league.url, type(e), e, format_exc()))
            self.errors += 1
        else:
            self.logger.info('[+] scheduler {}: OK'.format(league.url))
            self.tasks_done += 1


if __name__ == '__main__':
    UpdateTVSchedule()
