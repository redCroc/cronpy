import os

MYSQL_HOST = os.getenv('MYSQL_HOST')
MYSQL_USER = os.getenv('MYSQL_USER')
MYSQL_PASS = os.getenv('MYSQL_PASS')
MYSQL_BASE = os.getenv('MYSQL_BASE')
MYSQL_PORT = int(os.getenv('MYSQL_PORT', 0))
MYSQL_SAVE = True

REDIS_HOST = os.getenv('REDIS_HOST')
REDIS_PORT = int(os.getenv('REDIS_PORT', 0))
REDIS_BASE = int(os.getenv('REDIS_BASE', 0))

INFLUX_HOST = os.getenv('INFLUX_HOST')
INFLUX_PORT = int(os.getenv('INFLUX_PORT', 0))
INFLUX_BASE = int(os.getenv('INFLUX_BASE'))

LOG_LEVEL = 'INFO'

PROXY = 'http://127.0.0.1:8118'
SEMAPHORE = 20
WORKERS = 3

IMAGES_FOLDER = os.getenv('IMAGES_FOLDER')
GECKODRIVER_PATH = '/usr/local/bin/geckodriver'
FIREFOX_PATH = '/usr/local/bin/firefox'

ID_LEAGUE = None
ID_MATCH = None
ID_TEAM = None
ID_USER = None
ARGV = None

try:
    from setting_local import *
except ImportError:
    pass
