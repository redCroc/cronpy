from traceback import format_exc

from providers.controller import ProviderController
from lib.match import Match, Scheduler
from core.webutils import WebUtils


class UpdateSchedule(WebUtils):

    def __init__(self):
        super().__init__(module_='update_schedule')
        schedulers = self.get_schedulers()
        self.nb_tasks = len(schedulers)
        self.start()
        self.run(schedulers, self.update_schedule)
        self.end()

    def get_schedulers(self):
        schedulers = list()
        for match in Match.get_matches(origin=self.module, id_league=self.args.id_league, db=self.mysqldb):
            provider = ProviderController.get_provider(match.url)
            url = provider.get_schedule_url(match)

            for _scheduler in schedulers:               # Check if scheduler is already existing
                if _scheduler.url == url:
                    scheduler = _scheduler
                    break
            else:                                       # Else create new scheduler
                scheduler = Scheduler(url=url)
                schedulers.append(scheduler)
            scheduler.matches.append(match)             # Append match to scheduler
        return schedulers

    def update_schedule(self, scheduler, data):
        try:
            provider = ProviderController.get_provider(scheduler.url)
            provider.get_schedule(scheduler, data)
            for match in scheduler.matches:
                if match.task_done:
                    if match.new_url not in (None, match.url):
                        match.url = match.new_url
                        match.store_url(db=self.mysqldb)
                    if match.new_start_date not in (None, match.start_date):
                        match.start_date = match.new_start_date
                        match.store_start_date(db=self.mysqldb)
                    self.logger.info('[+] match {}: OK'.format(match.id))
                    self.quantity += 1

        except BaseException as e:
            self.logger.error('[-] scheduler {}: {} - {}\n{}'.format(scheduler.url, type(e), e, format_exc()))
            self.errors += 1
        else:
            self.logger.info('[+] scheduler {}: OK'.format(scheduler.url))
            if scheduler.recursive_id == 0:
                self.tasks_done += 1

        matches_not_done = [match for match in scheduler.matches if not match.task_done]
        if scheduler.recursive_id < 1 and len(matches_not_done) > 0:
            schedulers = list()
            if scheduler.previous_url is not None:
                previous_scheduler = Scheduler(url=scheduler.previous_url, recursive_id=scheduler.recursive_id+1)
                previous_scheduler.matches = matches_not_done
                schedulers.append(previous_scheduler)
            if scheduler.next_url is not None:
                next_scheduler = Scheduler(url=scheduler.next_url, recursive_id=scheduler.recursive_id + 1)
                next_scheduler.matches = matches_not_done
                schedulers.append(next_scheduler)
            if len(schedulers) > 0:
                self.run(schedulers, self.update_schedule)


if __name__ == '__main__':
    UpdateSchedule()
