from traceback import format_exc

from providers.controller import ProviderController
from core.webutils import WebUtils
from lib.tvchannel import TvChannel


class UpdateTvChannels(WebUtils):
    def __init__(self):
        super().__init__(module_='update_tvchannels')
        sources = list(TvChannel.get_sources())
        self.nb_tasks = len(sources)
        self.start()
        self.run(sources, self.update_tvchannels)
        self.end()

    def update_tvchannels(self, source, data):
        try:
            provider = ProviderController.get_provider(source.url)
            provider.get_tvchannels(source, data)
            for tv_channel in source.tv_channels:
                tv_channel.store(db=self.mysqldb)

        except BaseException as e:
            source.error = format_exc()
            self.logger.error('[-] team {}: {} - {}\n{}'.format(source.url, type(e), e, source.error))
            self.errors += 1
        else:
            source.error = None
            self.logger.info('[+] team {}: OK'.format(source.url))
            self.tasks_done += 1
            self.quantity += 1


if __name__ == '__main__':
    UpdateTvChannels()
