from datetime import datetime
from traceback import format_exc
import copy

from providers.controller import ProviderController
from lib.match import Event, Match
from core.webutils import WebUtils


class UpdateScores(WebUtils):

    def __init__(self):
        super().__init__(module_='update_scores')
        matches = list(Match.get_matches(
            origin=self.module, id_match=self.args.id_match, id_league=self.args.id_league, db=self.mysqldb
        ))
        matches_comms = list()
        for match in matches:
            if match.url_score is not None:
                match.url = match.url_score
                match.json_parser = True
            if match.url_comms is not None:
                match_comms = copy.copy(match)
                match_comms.url = match_comms.url_comms
                match_comms.json_parser = True
                matches_comms.append(match_comms)

        self.nb_tasks = len(matches) + len(matches_comms)
        self.start()
        self.run(matches, self.update_match)
        self.run(matches_comms, self.update_match_comms)
        self.end()

    def update_match(self, match, data):
        try:
            old_status = match.status
            old_nb_squad = len(match.squad) if match.squad is not None else 0
            old_nb_events = len(match.events) if match.events is not None else 0
            match.squad = list()
            match.events = list()

            # Get info from source
            provider = ProviderController.get_provider(match.url)
            provider.get_match_info(match, data)

            # Get new status
            match.get_new_status()

            # If match has just started update coeffs
            if match.status not in (match.COMING, match.POSTPONED) and match.league.bets and match.coeffs['home'] == 0:
                match.update_coeffs(self.mysqldb)

            # If match is in progress update teams ranking
            if old_status not in (match.OVER, match.WAITING_SCORERS) and match.status != match.COMING:
                match.update_teams_ranking(self.mysqldb)

            # If match has just finished update end_date
            if old_status not in (match.OVER, match.WAITING_SCORERS) and match.status == match.WAITING_SCORERS:
                match.store_end_date(self.mysqldb)

            # If match is totally over update users points
            if old_status != match.OVER and match.status == match.OVER:
                match.update_users_league(self.mysqldb)
                match.update_users_europe(self.mysqldb)

            # If match is over set winner
            if match.status in (match.OVER, match.WAITING_SCORERS):
                match.set_winner()

            # Set last_event if necessary
            if match.status == match.COMING and old_nb_squad == 0 and len(match.squad) > 0:
                match.last_event = Event(type_='squad')
            elif old_status == match.COMING and match.status == match.FIRST_TIME:
                match.last_event = Event(type_='start')
            elif old_status == match.FIRST_TIME and match.status == match.HALF_TIME:
                match.last_event = Event(type_='half_time')
            elif old_status == match.SECOND_TIME and match.status == match.OVER:
                match.last_event = Event(type_='over')
            elif len(match.events) > old_nb_events and match.events[-1].type == 'goal':
                match.last_event = match.events[-1]

            match.store_score(self.mysqldb)  # In any case store match details

        except BaseException as e:
            match.error = format_exc()
            self.logger.error('[-] match {}: {} - {}\n{}'.format(match.id, type(e), e, match.error))
            self.errors += 1
        else:
            match.error = None
            self.logger.info('[+] match {}: OK'.format(match.id))
            self.tasks_done += 1
            self.quantity += 1
        finally:
            if match.start_date < datetime.now() and match.status == match.COMING and match.sport.id != 2:
                match.status = 1
                match.minute = ''
                match.store_minute(self.mysqldb)
            match.store_error(self.mysqldb)

    def update_match_comms(self, match, data):
        try:
            # Get info from source
            provider = ProviderController.get_provider(match.url)
            provider.get_match_comms(match, data)

            provider.get_match_comms(match, data)
            if match.comms:
                match.store_comms(self.mysqldb)

        except BaseException as e:
            match.error = format_exc()
            self.logger.error('[-] match {}: {} - {}\n{}'.format(match.id, type(e), e, match.error))
            self.errors += 1
        else:
            match.error = None
            self.logger.info('[+] match {}: OK'.format(match.id))
            self.tasks_done += 1
            self.quantity += 1
        finally:
            match.store_error(self.mysqldb)


if __name__ == '__main__':
    UpdateScores()
