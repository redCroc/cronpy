from base64 import b64encode
from os import listdir

from setting import IMAGES_FOLDER

header = """<?xml version="1.0" encoding="UTF-8" standalone="no"?>"""
start_tag = """
<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:xlink="http://www.w3.org/1999/xlink"
   version="1.1"
   width="{size}"
   height="{size}"
   id="svg2">
"""
end_tag = """</svg>"""


for filename in listdir('{}/team/'.format(IMAGES_FOLDER)):
    if filename[-4:] == '.png':
        png_name = '{}/team/{}'.format(IMAGES_FOLDER, filename)
        with open(png_name, 'rb') as png_file:
            b64res = b64encode(png_file.read()).decode()
        image_tag = """
        <image xlink:href="data:image/png;base64,{b64res}" width="{size}" height="{size}" x="0" y="0" />
        """

        for size in (30, 50, 80):
            svg_name = '{}/team/h{}-'.format(IMAGES_FOLDER, size) + filename.replace('.png', '.svg')
            svg_res = header + start_tag.format(size=size) + image_tag.format(size=size, b64res=b64res) + end_tag
            with open(svg_name, 'w') as svg_file:
                svg_file.write(svg_res)


for filename in listdir('{}/league/'.format(IMAGES_FOLDER)):
    if filename[-4:] == '.png':
        png_name = '{}/league/{}'.format(IMAGES_FOLDER, filename)
        with open(png_name, 'rb') as png_file:
            b64res = b64encode(png_file.read()).decode()
        image_tag = """
        <image xlink:href="data:image/png;base64,{b64res}" width="{size}" height="{size}" x="0" y="0" />
        """

        for size in (35, 50, 80):
            svg_name = '{}/league/h{}-'.format(IMAGES_FOLDER, size) + filename.replace('.png', '.svg')
            svg_res = header + start_tag.format(size=size) + image_tag.format(size=size, b64res=b64res) + end_tag
            with open(svg_name, 'w') as svg_file:
                svg_file.write(svg_res)
                
          
for filename in listdir('{}/logo/'.format(IMAGES_FOLDER)):
    if filename == 'bob-logo.png':
        png_name = '{}/logo/{}'.format(IMAGES_FOLDER, filename)
        with open(png_name, 'rb') as png_file:
            b64res = b64encode(png_file.read()).decode()
        image_tag = """
        <image xlink:href="data:image/png;base64,{b64res}" width="{size}" height="{size}" x="0" y="0" />
        """

        for size in (40, 110):
            svg_name = '{}/logo/h{}-'.format(IMAGES_FOLDER, size) + filename.replace('.png', '.svg')
            svg_res = header + start_tag.format(size=size) + image_tag.format(size=size, b64res=b64res) + end_tag
            with open(svg_name, 'w') as svg_file:
                svg_file.write(svg_res)
