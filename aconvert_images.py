from urllib.request import urlopen
from os import listdir

from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By

from lib.tvchannel import TvChannel
from core.webutils import WebUtils
from lib.country import Country
from lib.browser import Browser
from lib.league import League
from lib.team import Team
import setting


class ConvertForm:
    def __init__(self, browser, choice):
        self.form = browser.find_element_by_xpath("//form[@id='conversionform']")

        self.form.find_element_by_xpath("//button[@class='btn glow trigger2 dropdown-toggle']").click()
        self.ul = self.form.find_element_by_xpath("//ul[@class='file-menu dropdown-menu']")
        self.ul.find_elements_by_tag_name('a')[1].click()
        self.file = self.form.find_element_by_xpath("//input[@id='file']")

        Select(self.form.find_element_by_xpath("//select[@id='targetformat']")).select_by_value('svg')
        if choice == 'tvchannels':
            Select(self.form.find_element_by_xpath("//select[@id='imagesize']")).select_by_value('option4')
            browser.execute_script("document.getElementsByName('preserveaspect')[0].click()")
        else:
            Select(self.form.find_element_by_xpath("//select[@id='imagesize']")).select_by_value('option2')

        self.size = self.form.find_element_by_xpath("//input[@id='customsize']")
        self.submit = self.form.find_element_by_xpath("//input[@id='submitbtn']")


def png2svg(browser, cform, png_url, svg_path, width, height):
    cform.file.clear()
    cform.file.send_keys(png_url)
    cform.size.clear()
    if width is None:
        cform.size.send_keys(str(height))
    else:
        cform.size.send_keys('{}x{}'.format(width, height))
    cform.submit.click()

    result_tr = WebDriverWait(browser, 30).until(
        expected_conditions.presence_of_element_located((By.XPATH, "//tbody[@id='resultbody']//tr"))
    )
    result_tds = result_tr.find_elements_by_tag_name('td')
    request = urlopen(url=result_tds[1].find_element_by_tag_name('a').get_attribute('href'))
    with open(svg_path, 'w') as svg_file:
        svg_file.write(request.read().decode())
    result_tds[3].find_element_by_xpath("//a[@title='Delete']").click()


class AconvertImages(WebUtils):
    def __init__(self):
        super().__init__(module_='aconvert_images')
        self.convert_images(choice=self.args.choice, head=self.args.head)

    def convert_images(self, choice, head):
        self.logger.info('[*] Starting job {}'.format(self.module))
        user_agent = self.mysqldb.get_random_ua()
        browser = Browser(user_agent=user_agent, headless=not head, tor=True)

        try:
            browser.get('https://check.torproject.org/?lang=fr')
            if browser.title.strip() == 'Félicitations. Ce navigateur est configuré pour utiliser Tor.':
                browser.execute_script('window.stop();')
                browser.get('https://www.aconvert.com/image/')
                convert_form = ConvertForm(browser, choice)

                if choice == 'leagues':
                    svg_folder = '{}/league'.format(setting.IMAGES_FOLDER)
                    png_list = listdir('/tmp')
                    for league in League.get_leagues(self.mysqldb):
                        png_name = '{}.png'.format(league.id)
                        if png_name in png_list:
                            self.logger.info('[+] League {} : {}'.format(league.id, league.name))
                            png_url = 'https://www.bestofbets.net/images/league/{}.png'.format(league.id)
                            for size in (35, 50, 80):
                                svg_path = '{}/h{}-{}.svg'.format(svg_folder, size, league.id)
                                png2svg(browser, convert_form, png_url, svg_path, size, size)

                elif choice == 'teams':
                    svg_folder = '{}/team'.format(setting.IMAGES_FOLDER)
                    png_list = listdir('/tmp')
                    for team in Team.get_teams(self.mysqldb):
                        png_name = '{}.png'.format(team.id)
                        if png_name in png_list:
                            self.logger.info('[+] Team {} : {}'.format(team.id, team.name))
                            png_url = 'https://www.bestofbets.net/images/team/{}.png'.format(team.id)
                            for size in (30, 50, 80):
                                svg_path = '{}/h{}-{}.svg'.format(svg_folder, size, team.id)
                                png2svg(browser, convert_form, png_url, svg_path, size, size)

                elif choice == 'countries':
                    svg_folder = '{}/country'.format(setting.IMAGES_FOLDER)
                    png_list = listdir('/tmp')
                    for country in Country.get_countries(self.mysqldb):
                        png_name = '{}.png'.format(country.id)
                        if png_name in png_list:
                            self.logger.info('[+] Country {} : {}'.format(country.id, country.name))
                            png_url = 'https://www.bestofbets.net/images/country/{}.png'.format(country.id)
                            for size in (30, 50, 80):
                                svg_path = '{}/h{}-{}.svg'.format(svg_folder, size, country.id)
                                png2svg(browser, convert_form, png_url, svg_path, size, size)

                elif choice == 'logos':
                    svg_folder = '{}/logo'.format(setting.IMAGES_FOLDER)
                    png_list = listdir('/tmp')
                    png_name = 'bob-logo.png'
                    if png_name in png_list:
                        png_url = 'https://www.bestofbets.net/images/logo/bob-logo.png'
                        for width, height in ((32, 40), (89, 110)):
                            svg_path = '{}/h{}-bob-logo.20svg'.format(svg_folder, height)
                            png2svg(browser, convert_form, png_url, svg_path, width, height)

                elif choice == 'tvchannels':
                    svg_folder = '{}/tvchannel'.format(setting.IMAGES_FOLDER)
                    png_list = listdir('/tmp')
                    for tvchannel in TvChannel.get_tvchannels(self.mysqldb):
                        png_name = '{}.png'.format(tvchannel.id)
                        if png_name in png_list:
                            self.logger.info('[+] TVChannel {} : {}'.format(tvchannel.id, tvchannel.name))
                            png_url = 'https://www.bestofbets.net/images/tvchannel/{}.png'.format(tvchannel.id)
                            for height in (20, 30):
                                svg_path = '{}/h{}-{}.svg'.format(svg_folder, height, tvchannel.id)
                                png2svg(browser, convert_form, png_url, svg_path, None, height)

        except BaseException as e:
            self.logger.error('[-] error : {} - {}'.format(type(e), str(e)))
        finally:
            browser.quit()
            self.logger.info('[*] Job done {}'.format(self.module))


if __name__ == '__main__':
    AconvertImages()
