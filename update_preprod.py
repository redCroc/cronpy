from traceback import format_exc
import sys

from core.webutils import WebUtils


class UpdatePreprod(WebUtils):

    def __init__(self):
        super().__init__(module_='update_preprod')
        tables = list(self.mysqldb.get_tables(self.args.table))
        self.nb_tasks = len(tables)
        self.start()
        self.update_tables(tables)
        self.end()

    def update_tables(self, tables):
        for table in tables:
            try:
                self.mysqldb.exec('DROP TABLE IF EXISTS preprod.{table}'.format(table=table))
                self.mysqldb.exec('CREATE TABLE preprod.{table} LIKE bob.{table}'.format(table=table))
                self.mysqldb.exec('INSERT INTO preprod.{table} SELECT * FROM bob.{table}'.format(table=table))
            except BaseException as e:
                self.logger.error('[-] table {}: {} - {}\n{}'.format(table, type(e), e, format_exc()))
                self.errors += 1
            else:
                self.logger.info('[+] table {}: OK'.format(table))
                self.tasks_done += 1
                self.quantity += 1


if __name__ == '__main__':
    UpdatePreprod()
