from argparse import ArgumentParser

import setting


class ArgParser(ArgumentParser):

    def __init__(self):
        super().__init__()
        self.add_argument('-il', '--id-league', type=int, help='League id')
        self.add_argument('-im', '--id-match', type=int, help='Match id')
        self.add_argument('-it', '--id-team', type=int, help='Team id')
        self.add_argument('-iu', '--id-user', type=int, help='User id')
        self.add_argument('-t', '--table', type=str, help='Table name')
        self.add_argument('-c', '--choice', type=str, help='Choice for aconvert')
        self.add_argument('-hd', '--head', action='store_true', help='Browser with head')

    def parse_args(self, args=None, namespace=None):
        parsed = super().parse_args()
        parsed.id_league = setting.ID_LEAGUE if parsed.id_league is None else parsed.id_league
        parsed.id_match = setting.ID_MATCH if parsed.id_match is None else parsed.id_match
        parsed.id_team = setting.ID_TEAM if parsed.id_team is None else parsed.id_team
        parsed.id_user = setting.ID_USER if parsed.id_user is None else parsed.id_user
        return parsed
