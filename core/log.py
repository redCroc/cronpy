import logging.handlers
import sys

import setting


def get_logger(name, level=setting.LOG_LEVEL):
    logger = logging.getLogger(name)
    formatter = logging.Formatter('[%(asctime)s] %(levelname)s / %(name)s : %(message)s')
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    handler.setFormatter(formatter)
    logger.setLevel(getattr(logging, level))
    logger.addHandler(handler)
    return logger
