import influxdb

import setting


class InfluxDB:

    def __init__(self, host=setting.INFLUX_HOST, port=setting.INFLUX_PORT, base=setting.INFLUX_BASE):
        self.host = host
        self.port = port
        self.base = base
        self._client = influxdb.InfluxDBClient(host=self.host, port=self.port, database=self.base)

    def save_stats(self, module, start_time, nb_tasks, tasks_done, quantity, errors, total_time):
        points = [{
            'measurement': module,
            'time': start_time,
            'fields': {
                'nbt': nb_tasks,
                'tdn': tasks_done,
                'qty': quantity,
                'err': errors,
                'tti': total_time
            }
        }]
        self._client.write_points(points, time_precision='s')
