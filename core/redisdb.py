import redis

import setting


class RedisDB:

    DEFAULT_EXPIRE = 300

    def __init__(self, host=setting.REDIS_HOST, port=setting.REDIS_PORT, base=setting.REDIS_BASE):
        self.host = host
        self.port = port
        self.base = base
        self._client = redis.StrictRedis(host=self.host, port=self.port, db=self.base, retry_on_timeout=True)

    def save_start(self, module, expire=300):
        lock_key = 'LOCK:{}'.format(module)
        self._client.ping()
        if self._client.get(lock_key) is not None:
            return False
        else:
            self._client.set(lock_key, 1)
            self._client.expire(lock_key, expire)
            return True

    def save_end(self, module):
        lock_key = 'LOCK:{}'.format(module)
        self._client.ping()
        self._client.delete(lock_key)
