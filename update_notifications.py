from traceback import format_exc
from datetime import timedelta

from lib.notification import Notification
from core.webutils import WebUtils
from lib.user import User


class UpdateNotifications(WebUtils):

    def __init__(self):
        super().__init__(module_='update_notifications')
        league_users = list(User.get_users_league_bets(db=self.mysqldb))
        europe_users = list(User.get_users_europe_bets(db=self.mysqldb))
        self.nb_tasks = len(league_users) + len(europe_users)
        self.start()
        self.update_league_notifications(league_users)
        self.update_europe_notifications(europe_users)
        self.end()

    def update_league_notifications(self, users):
        self.logger.info('[*] Update users league notifications')
        for user in users:
            if user.notifications is not None and 'league' is user.notifications:
                try:
                    for mday, matches in user.league_bets_matches.items():
                        if matches:
                            for method, hours in user.notifications['league'].items():
                                for hour in hours:
                                    date_diff = matches[0].start_date - timedelta(hours=hour)
                                    notif = Notification(
                                        id_user=user.id,
                                        id_match=matches[0].id,
                                        date=date_diff,
                                        mday_or_round=str(mday),
                                        type_='league',
                                        method=method
                                    )
                                    notif.save(db=self.mysqldb)

                except BaseException as e:
                    self.logger.error('[-] user {}: {} - {}\n{}'.format(user.id, type(e), e, format_exc()))
                    self.errors += 1
                else:
                    self.logger.info('[+] user {}: OK'.format(user.id))
                    self.tasks_done += 1
                    self.quantity += 1

    def update_europe_notifications(self, users):
        self.logger.info('[*] Update users europe notifications')
        for user in users:
            if user.notifications is not None and 'europe' is user.notifications:
                try:
                    matches = user.europe_bets_matches
                    if matches:
                        for method, hours in user.notifications['europe'].items():
                            for hour in hours:
                                date_diff = matches[0].start_date - timedelta(hours=hour)
                                notif = Notification(
                                    id_user=user.id,
                                    id_match=matches[0].id,
                                    date=date_diff,
                                    mday_or_round=str(user.europe_round),
                                    type_='europe',
                                    method=method
                                )
                                notif.save(db=self.mysqldb)

                except BaseException as e:
                    self.logger.error('[-] user {}: {} - {}\n{}'.format(user.id, type(e), e, format_exc()))
                    self.errors += 1
                else:
                    self.logger.info('[+] user {}: OK'.format(user.id))
                    self.tasks_done += 1
                    self.quantity += 1


if __name__ == '__main__':
    UpdateNotifications()
