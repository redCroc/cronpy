from traceback import format_exc

from providers.controller import ProviderController
from core.webutils import WebUtils
from lib.team import Team


class UpdateStaff(WebUtils):

    def __init__(self):
        super().__init__(module_='update_staff')
        teams = list(Team.get_teams(origin=self.module, id_team=self.args.id_team, db=self.mysqldb))
        self.nb_tasks = len(teams)
        self.start()
        self.run(teams, self.update_staff)
        self.end()

    def update_staff(self, team, data):
        try:
            provider = ProviderController.get_provider(team.url)
            new_staff = provider.get_team_staff(data)
            if new_staff != team.staff:
                team.staff = new_staff
                team.store_staff(self.mysqldb)

        except BaseException as e:
            team.error = format_exc()
            self.logger.error('[-] team {}: {} - {}\n{}'.format(team.id, type(e), e, team.error))
            self.errors += 1
        else:
            team.error = None
            self.logger.info('[+] team {}: OK'.format(team.id))
            self.tasks_done += 1
            self.quantity += 1
        finally:
            team.store_error(self.mysqldb)


if __name__ == '__main__':
    UpdateStaff()
