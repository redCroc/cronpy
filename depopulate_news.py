from datetime import date, timedelta
import os.path

from lib.tools import url_sanitize
from core.webutils import WebUtils
from lib.news import News
import setting


class DepopulateNews(WebUtils):

    def __init__(self):
        super().__init__(module_='remove_news')
        deadline = date.today() - timedelta(days=90)
        old_newss = list(News.get_older_news(date_limit=deadline, db=self.mysqldb))
        self.nb_tasks = len(old_newss)
        self.start()
        self.remove_old_newss(newss=old_newss)
        self.end()

    def remove_old_newss(self, newss):
        for news in newss:
            self.logger.info('[+] remove news {}'.format(news.id))
            news.remove(db=self.mysqldb)
            self.quantity += 1
            if url_sanitize(news.image.title) in news.image.basename:
                img_path = '{}/news/{}'.format(setting.IMAGES_FOLDER, news.image.basename)
                if os.path.exists(img_path):
                    os.remove(img_path)
            self.tasks_done += 1
        News.update_news_counters(db=self.mysqldb)


if __name__ == '__main__':
    DepopulateNews()
