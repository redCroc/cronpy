def n2v(x):
    if x is None:
        return ''
    return x


def void(x):
    return x


def unity(x):
    if x > 0:
        return 1
    elif x < 0:
        return -1
    else:
        return 0


def real_round(x, n=1):
    res = 0
    interval = 1 / (10 ** n)
    while True:
        if x < res + interval/2 - interval/20:
            break
        res += interval
    return round(res, n)


def url_sanitize(s):
    res = ''
    for c in s:
        if ord('a') <= ord(c) <= ord('z') or ord('0') <= ord(c) <= ord('9'):
            res += c
        elif ord('A') <= ord(c) <= ord('Z'):
            res += chr(ord(c) - ord('A') + ord('a'))
        elif c in ('è', 'é', 'ê', 'ë', 'É'):
            res += 'e'
        elif c in ('à', 'á', 'â', 'ä', 'ã'):
            res += 'a'
        elif c in ('ò', 'ó', 'ô', 'ö', 'ø'):
            res += 'o'
        elif c in ('ì', 'í', 'î', 'ï', 'Î'):
            res += 'i'
        elif c in ('ù', 'ú', 'û', 'ü'):
            res += 'u'
        elif c == 'ñ':
            res += 'n'
        elif c in ('ç', 'ć'):
            res += 'c'
        elif c == 'š':
            res += 's'
        elif c == 'ý':
            res += 'y'
        elif c == 'ž':
            res += 'z'
        else:
            res += '-'
    return res
