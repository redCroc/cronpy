import json

from lib.country import Country


class Team:
    def __init__(self, idt):
        self.id = idt
        self.name = None
        self.short_name = None
        self.long_name = None
        self.names = dict()
        self.images = dict()
        self.staff = dict()
        self.error = None
        self.league = None
        self.country = None
        self.id_sport = None
        self.url = None

        self.group = None
        self.rank = 0
        self.played = 0
        self.points = 0
        self.wins = 0
        self.ties = 0
        self.loss = 0
        self.g_for = 0
        self.g_against = 0
        self.g_diff = 0
        self.coeff = 0

    def store(self, db):
        stmt = """
        INSERT IGNORE INTO teams (id_sport, name, id_country, short_name, long_name, gender, names, url, staff, images)
        VALUES (:id_sport, :name, :id_country, :short_name, :long_name, :gender, :names, :url, :staff, :images)
        """
        args = {'id_sport': self.league.sport.id, 'name': self.name, 'id_country': self.country.id,
                'short_name': self.short_name, 'long_name': self.long_name, 'gender': self.league.gender,
                'names': json.dumps(self.names, separators=(',', ':')), 'url': self.url,
                'staff': json.dumps(self.staff, separators=(',', ':')),
                'images': json.dumps(self.images, separators=(',', ':'))}
        self.id = db.exec(stmt, args)
        if self.id == 0:
            stmt = 'SELECT id FROM teams WHERE id_sport = :id_sport AND name = :name'
            args = {'id_sport': self.league.sport.id, 'name': self.name}
            for row in db.query(stmt, args):
                self.id = row['id']
        db.exec(
            'INSERT IGNORE INTO league_teams (id_team, id_league) VALUES (:id_team, :id_league)',
            {'id_team': self.id, 'id_league': self.league.id}
        )

    def reset_live_stats(self, db):
        stmt = """
        UPDATE league_teams SET played_live = played, pts_live = pts, wins_live = wins, ties_live = ties,
        loss_live = loss, gf_live = gf, ga_live = ga, diff_live = diff
        WHERE id_team = :id_team AND id_league = :id_league
        """
        args = {'id_team': self.id, 'id_league': self.league.id}
        db.exec(stmt, args)

    def update_live_stats(self, db):
        stmt = """
        UPDATE league_teams SET played_live = played + :played, pts_live = pts + :pts, wins_live = wins + :wins,
        ties_live = ties + :ties, loss_live = loss + :loss, gf_live = gf + :gf, ga_live = ga + :ga,
        diff_live = diff + :diff
        WHERE id_team = :id_team AND id_league = :id_league
        """
        args = {'played': self.played, 'pts': self.points, 'wins': self.wins, 'ties': self.ties, 'loss': self.loss,
                'gf': self.g_for, 'ga': self.g_against, 'diff': self.g_diff, 'id_team': self.id,
                'id_league': self.league.id}
        db.exec(stmt, args)

    def update_final_stats(self, db):
        stmt = """
        UPDATE league_teams SET played = played + :played, pts = pts + :pts, wins = wins + :wins, ties = ties + :ties,
        loss = loss + :loss, gf = gf + :gf, ga = ga + :ga, diff = diff + :diff
        WHERE id_team = :id_team AND id_league = :id_league
        """
        args = {'played': self.played, 'pts': self.points, 'wins': self.wins, 'ties': self.ties, 'loss': self.loss,
                'gf': self.g_for, 'ga': self.g_against, 'diff': self.g_diff, 'id_team': self.id,
                'id_league': self.league.id}
        db.exec(stmt, args)

    def set_stats(self, db):
        stmt = """
        UPDATE league_teams SET `group` = :group, played = :played, played_live = :played, pts = :pts, pts_live = :pts, 
        wins = :wins, wins_live = :wins, ties = :ties, ties_live = :ties, loss = :loss, loss_live = :loss, gf = :gf, 
        gf_live = :gf, ga = :ga, ga_live = :ga, diff = :diff, diff_live = :diff, rank = :rank, rank_live = :rank, 
        rank_old = :rank
        WHERE id_team = :id_team AND id_league = :id_league
        """
        args = {'group': self.group.name if self.group is not None else '', 'played': self.played, 'pts': self.points,
                'wins': self.wins, 'ties': self.ties, 'loss': self.loss, 'gf': self.g_for, 'ga': self.g_against,
                'diff': self.g_diff, 'id_team': self.id, 'id_league': self.league.id, 'rank': self.rank}
        db.exec(stmt, args)

    def store_staff(self, db):
        stmt = """
        UPDATE teams
        SET staff = :staff
        WHERE id = :id
        """
        args = {'staff': json.dumps(self.staff, separators=(',', ':')), 'id': self.id}
        db.exec(stmt, args)

    def store_images(self, db):
        stmt = """
        UPDATE teams
        SET images = :images
        WHERE id = :id
        """
        args = {'images': json.dumps(self.images, separators=(',', ':')), 'id': self.id}
        db.exec(stmt, args)

    def store_names_and_url(self, db):
        stmt = """
        UPDATE teams
        SET names = :names, url = :url
        WHERE id = :id
        """
        args = {'names': json.dumps(self.names, separators=(',', ':')), 'url': self.url, 'id': self.id}
        db.exec(stmt, args)

    def store_error(self, db):
        stmt = """
        UPDATE teams SET error = :error WHERE id = :id
        """
        args = {'error': self.error, 'id': self.id}
        db.exec(stmt, args)

    @staticmethod
    def get_teams(db, origin=None, id_team=None, url=None):
        where_conditions = list()
        args = dict()
        if origin in ('update_staff', 'update_players'):
            where_conditions.append("url IS NOT NULL")
        if id_team is not None:
            where_conditions.append('id = :id'.format(int(id_team)))
            args['id'] = int(id_team)
        if url is not None:
            where_conditions.append('url LIKE :url')
            args['url'] = url

        where_clause = ''
        if len(where_conditions) > 0:
            where_clause = 'WHERE {}'.format(' AND '.join(where_conditions))

        stmt = """
        SELECT id, name, id_sport, id_country, images, url, staff
        FROM teams 
        {}
        """.format(where_clause)
        for row in db.query(stmt, args):
            team = Team(row['id'])
            team.name = row['name']
            team.id_sport = row['id_sport']
            team.country = Country(idt=row['id_country'])
            team.images = json.loads(row['images'])
            team.url = row['url']
            team.staff = json.loads(row['staff'])
            yield team

    @staticmethod
    def from_source(names, league, db):
        stmt = """
        SELECT teams.id, teams.name
        FROM teams
        INNER JOIN league_teams ON league_teams.id_team = teams.id
        WHERE JSON_CONTAINS(teams.names, :names) AND teams.id_sport = :id_sport AND teams.gender = :gender 
            AND league_teams.id_league = :id_league
        """
        args = {
            'names': json.dumps(names), 'id_sport': league.sport.id, 'gender': league.gender, 'id_league': league.id
        }
        for row in db.query(stmt, args):
            team = Team(row['id'])
            team.name = row['name']
            return team

        stmt = """
        SELECT teams.id, teams.name
        FROM teams
        WHERE JSON_CONTAINS(teams.names, :names) AND teams.id_sport = :id_sport AND teams.gender = :gender 
        """
        args = {'names': json.dumps(names), 'id_sport': league.sport.id, 'gender': league.gender}
        for row in db.query(stmt, args):
            team = Team(row['id'])
            team.name = row['name']
            stmt = 'INSERT IGNORE INTO league_teams (id_league, id_team) VALUES (:id_league, :id_team)'
            args = {'id_league': league.id, 'id_team': team.id}
            db.exec(stmt, args)
            return team

        return None
