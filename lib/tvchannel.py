import json


class TvChannelSource:

    SOURCES = {
        3: 'http://www.programme-television.org/chaines-tv'
    }

    def __init__(self, id_country, url):
        self.id_country = id_country
        self.url = url
        self.tv_channels = list()
        self.error = None


class TvChannel:
    def __init__(self, id_country, name, idt=0):
        self.id = idt
        self.id_country = id_country
        self.name = name
        self.names = None

    def store(self, db):
        db.exec(
            'INSERT IGNORE INTO tv_channels (id_country, name) VALUES (:id_country, :name)',
            {'id_country': self.id_country, 'name': self.name}
        )

    @staticmethod
    def get_sources():
        for id_country, url in TvChannelSource.SOURCES.items():
            yield TvChannelSource(id_country, url)

    @staticmethod
    def get_tvchannels(db):
        for row in db.query('SELECT id, id_country, name, names FROM tv_channels'):
            tv_channel = TvChannel(id_country=row['id_country'], name=row['name'], idt=row['id'])
            tv_channel.names = json.loads(row['names'])
            yield tv_channel
