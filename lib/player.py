import datetime
import hashlib
import json
import time


class PlayerImage:
    def __init__(self, url, full_name, birth_date):
        self.url = url
        self.full_name = full_name
        self.birth_date = birth_date
        self.path = None
        self.last_save = 0
        self.last_modified = 0
        self.set_path()

    def set_lm(self, lm):
        if lm.isnumeric():
            self.last_modified = int(lm)

    def set_path(self):
        self.path = '{hash}.{ext}'.format(
            hash='{full_name}{birth_date}'.format(
                full_name=hashlib.md5(self.full_name.encode()).hexdigest(),
                birth_date=hashlib.md5(self.birth_date.strftime('%Y-%m-%d').encode()).hexdigest()
            ),
            ext=self.url.split('.')[-1].split('?')[0]
        )


class Player:

    ROLE_GOALKEEPER = 1
    ROLE_DEFENDER = 2
    ROLE_MIDFIELDER = 3
    ROLE_ATTACKER = 4

    FOOT_RIGHT = 1
    FOOT_LEFT = 2
    FOOT_BOTH = 3

    PRICE_MIN = 10000

    def __init__(self, idt=None, team=None, country1=None, country2=None, full_name=None, number=None, role=None,
                 position=None, birth_date=None, size=None, foot=None, contract_type=None, contract_end=None,
                 price=None, image_url=None, error=None):
        self.id = idt
        self.team = team
        self.country1 = country1
        self.country2 = country2
        self.first_name = None
        self.last_name = None
        self.full_name = None
        self.number = number
        self.role = role
        self.position = position
        self.birth_date = None
        self.age = None
        self.size = size
        self.foot = foot
        self.contract_type = contract_type
        self.contract_end = contract_end
        self.price = None
        self.image = None
        self.error = error

        self.set_names(full_name)
        self.set_age(birth_date)
        self.set_image(image_url)
        self.set_price(price)

    def set_names(self, full_name):
        self.full_name = full_name
        if self.full_name:
            names = self.full_name.split(' ')
            first_names = list()
            last_names = list()
            for idx in range(len(names)):
                if idx == len(names) - 1:
                    last_names.append(names[idx])
                elif last_names:
                    last_names.append(names[idx])
                elif len(names[idx]) < 4:
                    last_names.append(names[idx])
                else:
                    first_names.append(names[idx])
            self.first_name = ' '.join(first_names)
            self.last_name = ' '.join(last_names)

    def set_age(self, birth_date):
        self.birth_date = birth_date
        if self.birth_date is not None:
            delta = datetime.datetime.now() - self.birth_date
            self.age = int(delta.days / 365.25)

    def set_image(self, image_url):
        if image_url and self.full_name and self.birth_date:
            self.image = PlayerImage(url=image_url, full_name=self.full_name, birth_date=self.birth_date)

    def set_price(self, price):
        if price is not None:
            self.price = max(self.PRICE_MIN, price)
        else:
            self.price = self.PRICE_MIN

    def get_image_details(self, db):
        stmt = """SELECT image_save FROM players WHERE full_name = :full_name AND birth_date = :birth_date LIMIT 1"""
        args = {'full_name': self.full_name, 'birth_date': self.birth_date}
        for row in db.query(stmt, args):
            self.image.last_save = row['image_save']

    def store(self, db):
        stmt = """
        INSERT INTO players (id_team, id_sport, id_country1, id_country2, first_name, last_name, full_name, number, 
            role, position, birth_date, age, size, foot, contract_type, contract_end, price, error)
        VALUES (:id_team, :id_sport, :id_country1, :id_country2, :first_name, :last_name, :full_name, :number, :role, 
            :position, :birth_date, :age, :size, :foot, :contract_type, :contract_end, :price, :error)
        ON DUPLICATE KEY UPDATE id_team = :id_team, id_sport = :id_sport, id_country1 = :id_country1, 
            id_country2 = :id_country2, first_name = :first_name, last_name = :last_name, full_name = :full_name, 
            number = :number, role = :role, position = :position, birth_date = :birth_date, age = :age, size = :size, 
            foot = :foot, contract_type = :contract_type, contract_end = :contract_end, price = :price,
            error = :error
        """
        args = {
            'id_team': self.team.id if self.team is not None else None,
            'id_sport': self.team.id_sport if self.team is not None else None,
            'id_country1': self.country1.id if self.country1 is not None else None,
            'id_country2': self.country2.id if self.country2 is not None else None,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'full_name': self.full_name,
            'number': self.number,
            'role': self.role,
            'position': self.position,
            'birth_date': self.birth_date.strftime('%Y-%m-%d') if self.birth_date is not None else None,
            'age': self.age,
            'size': self.size,
            'foot': self.foot,
            'contract_type': self.contract_type,
            'contract_end': self.contract_end.strftime('%Y-%m-%d') if self.contract_end is not None else None,
            'price': self.price,
            'error': json.dumps(self.error) if self.error is not None else None
        }
        self.id = db.exec(stmt, args)

    def store_image(self, db):
        now = int(time.time())
        stmt = """
        UPDATE players SET image = :image, image_save = :image_save 
        WHERE full_name = :full_name AND birth_date = :birth_date
        """
        args = {
            'image': '{}?v={}'.format(self.image.path, now),
            'image_save': now,
            'full_name': self.full_name,
            'birth_date': self.birth_date
        }
        db.exec(stmt, args)

    def store_error(self, db):
        stmt = """UPDATE players SET error = :error WHERE full_name = :full_name AND birth_date = :birth_date"""
        args = {
            'error': json.dumps(self.error) if self.error is not None else None,
            'full_name': self.full_name,
            'birth_date': self.birth_date
        }
        db.exec(stmt, args)
