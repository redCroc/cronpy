import json


class Country:

    def __init__(self, idt, name=None, short_name=None, names=None):
        self.id = idt
        self.name = name
        self.short_name = short_name
        self.names = names

    def build_from_name(self, db):
        for row in db.query('SELECT id FROM countries WHERE name = :name', {'name': self.name}):
            self.id = row['id']

    @staticmethod
    def get_countries(db):
        for row in db.query("SELECT * FROM countries"):
            yield Country(
                idt=row['id'], name=row['name'], short_name=row['short_name'], names=json.loads(row['names'])
            )


if __name__ == '__main__':
    from core.mysqldb import MysqlDB
    d = MysqlDB()
    for c in Country.get_countries(d):
        print(c.__dict__)