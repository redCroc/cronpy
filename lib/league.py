import json
import sys

from lib.country import Country
from lib.team import Team
from lib.tools import n2v

if 'lib.match' not in sys.modules:
    from lib.match import Match


class Sport:
    def __init__(self, idt):
        self.id = idt
        self.name = None
        self.display_sets = None


class Group:
    def __init__(self, name, url, league):
        self.name = name
        self.url = url
        self.league = league


class League:
    def __init__(self, idt):
        self.id = idt
        self.name = None
        self.bets = None
        self.url = None
        self.gender = None
        self.images = dict()
        self.error = ''
        self.points = dict()
        self.teams = list()
        self.matches = list()
        self.tags = list()
        self.round_dates = None
        self.sport = None
        self.country = None

    def get_teams(self, db):
        stmt = """
        SELECT league_teams.id_team, teams.name, teams.names, teams.url
        FROM league_teams
        INNER JOIN teams ON teams.id = league_teams.id_team
        WHERE league_teams.id_league = :id_league
        """
        args = {'id_league': self.id}
        teams = list()
        for row in db.query(stmt, args):
            team = Team(row['id_team'])
            team.name = row['name']
            team.names = json.loads(row['names'])
            team.url = row['url']
            team.league = self
            teams.append(team)
        return teams

    def get_matches(self, db):
        stmt = """
        SELECT matches.id, matches.start_date, matches.id_home, matches.id_away, matches.url, matches.mday,
        home.name AS home_name, home.names AS home_names,
        away.name AS away_name, away.names AS away_names
        FROM matches
        INNER JOIN teams AS home ON home.id = matches.id_home
        INNER JOIN teams AS away ON away.id = matches.id_away
        WHERE matches.id_league = :id_league
        """
        for row in db.query(stmt, {'id_league': self.id}):
            match = Match(idt=row['id'])
            match.start_date = row['start_date']
            match.url = row['url']
            match.mday = row['mday']
            match.home = Team(idt=row['id_home'])
            match.home.name = row['home_name']
            match.home.names = json.loads(row['home_names'])
            match.away = Team(idt=row['id_away'])
            match.away.name = row['away_name']
            match.away.names = json.loads(row['away_names'])
            yield match

    def update_live_ranking(self, group, db):
        stmt = """
        SELECT id, rank_live
        FROM league_teams
        WHERE id_league = :id_league AND `group` = :group
        ORDER BY pts_live DESC, diff_live DESC, gf_live DESC
        """
        args = {'id_league': self.id, 'group': group}
        new_rank = 0
        for row in db.query(stmt, args):
            new_rank += 1
            if new_rank != row['rank_live']:
                stmt = """
                UPDATE league_teams SET rank_live = :rank WHERE id = :id
                """
                args = {'rank': new_rank, 'id': row['id']}
                db.exec(stmt, args)

    def update_final_ranking(self, group, db):
        stmt = """
        SELECT id, rank
        FROM league_teams
        WHERE id_league = :id_league AND `group` = :group
        ORDER BY pts DESC, diff DESC, gf DESC
        """
        args = {'id_league': self.id, 'group': group}
        new_rank = 0
        for row in db.query(stmt, args):
            new_rank += 1
            if new_rank != row['rank']:
                stmt = """
                UPDATE league_teams SET rank = :rank WHERE id = :id
                """
                args = {'rank': new_rank, 'id': row['id']}
                db.exec(stmt, args)

    def store_images(self, db):
        stmt = """
        UPDATE leagues
        SET images = :images
        WHERE id = :id
        """
        args = {'images': json.dumps(self.images, separators=(',', ':')), 'id': self.id}
        db.exec(stmt, args)

    def store_error(self, db):
        stmt = """
        UPDATE leagues SET error = :error WHERE id = :id
        """
        args = {'error': n2v(self.error), 'id': self.id}
        db.exec(stmt, args)

    def store_round(self, new_round, db):
        for row in db.query('SELECT rounds FROM leagues WHERE id = :id', {'id': self.id}):
            if new_round not in row['rounds'].split(','):
                rounds = '{},{}'.format(row['rounds'], new_round)
                db.exec('UPDATE leagues SET rounds = :rounds WHERE id = :id', {'rounds': rounds, 'id': self.id})

    def store_groups(self, groups, db):
        db.exec(
            'UPDATE leagues SET groups = :groups WHERE id = :id',
            {'groups': ','.join([group.name for group in groups]), 'id': self.id}
        )

    @staticmethod
    def get_leagues(db, origin=None, id_league=None, with_tags=False):
        where_conditions = list()
        args = dict()

        if origin == 'update_rankings':
            where_conditions.append('leagues.url_ranking IS NOT NULL')
        elif origin == 'update_schedule':
            where_conditions.append('leagues.url_schedule IS NOT NULL')
        elif origin == 'update_tvschedule':
            where_conditions.append('leagues.url_tvschedule IS NOT NULL')
        elif origin == 'create_schedule':
            where_conditions.append('leagues.url_schedule IS NOT NULL')
            where_conditions.append('leagues.auto_schedule = 1')

        if id_league is not None:
            where_conditions.append('leagues.id = :id_league')
            args['id_league'] = int(id_league)
        if with_tags:
            where_conditions.append('leagues.tags IS NOT NULL')

        where_clause = 'WHERE {}'.format(' AND '.join(where_conditions)) if len(where_conditions) > 0 else ''

        stmt = """
        SELECT id, name, points, url_schedule, url_ranking, url_tvschedule, images, id_sport, gender, tags, round_dates,
            id_country
        FROM leagues
        {}
        """.format(where_clause)
        for row in db.query(stmt, args):
            league = League(row['id'])
            league.name = row['name']
            league.points = json.loads(row['points'])
            league.images = json.loads(row['images'])
            league.sport = Sport(idt=row['id_sport'])
            league.gender = row['gender']
            league.tags = json.loads(row['tags'])
            league.round_dates = json.loads(row['round_dates']) if row['round_dates'] is not None else None
            league.country = Country(idt=row['id_country'])
            if origin == 'update_rankings':
                league.url = row['url_ranking']
            elif origin in ('update_schedule', 'create_schedule'):
                league.url = row['url_schedule']
            elif origin == 'update_tvschedule':
                league.url = row['url_tvschedule']
            yield league
