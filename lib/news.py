from lib.tools import url_sanitize
from lib.league import Sport
from lib.tools import n2v
import setting


class NewsImage:
    def __init__(self, url, title, basename, id_news):
        self.url = url
        self.title = title
        self.id_news = id_news
        if url is not None:
            ext = self.url.split('.')[-1]
            self.basename = '{}.{}'.format(url_sanitize(self.title), ext)
        else:
            self.basename = basename
        self.abspath = '{}/news/{}'.format(setting.IMAGES_FOLDER.rstrip('/'), self.basename)

    def store(self, db):
        db.exec(
            "UPDATE news SET image = :image WHERE id = :id",
            {'image': self.basename, 'id': self.id_news}
        )


class NewsSource:
    def __init__(self, idt, id_sport, url):
        self.id = idt
        self.url = url
        self.sport = Sport(id_sport)
        self.nb_news = 0
        self.data = None
        self.error = None

    def store_error(self, db):
        db.exec(
            "UPDATE news_source SET error = :error WHERE id = :id",
            {'error': self.error, 'id': self.id}
        )

    @staticmethod
    def get_sources(db):
        for row in db.query(""" SELECT id, id_sport, url FROM news_source """):
            yield NewsSource(idt=row['id'], id_sport=row['id_sport'], url=row['url'])


class News:
    def __init__(self):
        self.id = 0
        self.pub_date = None
        self.title = None
        self.description = None
        self.url = None
        self.image = None
        self.source = None
        self.tags = list()
        self.sport = None
        self.content = None
        self.teaser = None
        self.content = None
        self.author = None
        self.video_src = None
        self.data = None
        self.error = None
        self.league = None

    def store(self, db):
        haystack = url_sanitize(f'{self.title}|{self.description}')
        self.id = db.exec(
            """ 
                INSERT IGNORE INTO news 
                    (id_sport, id_league, id_team, pub_date, title, description, link, source, tags, image)
                VALUES (
                    :id_sport, (SELECT get_matching_league(:haystack, :id_sport)), 
                    (SELECT get_matching_team(:haystack, :id_sport)), :pub_date, 
                    :title, :description, :link, :source, :tags, :image
                )
            """,
            {
                'id_sport': self.sport.id,
                'haystack': haystack,
                'pub_date': self.pub_date.strftime('%Y-%m-%d %H:%M:%S'),
                'title': self.title,
                'description': self.description,
                'link': self.url,
                'source': self.source,
                'tags': ','.join(self.tags),
                'image': self.image.basename
            }
        )
        db.exec("""
            UPDATE leagues 
            SET leagues.nb_news = leagues.nb_news + 1 
            WHERE leagues.id = (SELECT news.id_league FROM news WHERE news.id = :id_news)
        """, {'id_news': self.id})
        db.exec("""
            UPDATE teams
            SET teams.nb_news = teams.nb_news + 1
            WHERE teams.id = (SELECT news.id_team FROM news WHERE news.id = :id_news)
        """, {'id_news': self.id})
        self.image.id_news = self.id

    def store_content(self, db):
        db.exec(
            "UPDATE news SET teaser = :teaser, author = :author, content = :content WHERE id = :id",
            {'teaser': n2v(self.teaser), 'author': n2v(self.author), 'content': n2v(self.content), 'id': self.id}
        )

    def store_error(self, db):
        db.exec(
            "UPDATE news SET error = :error, link = :url WHERE id = :id",
            {'error': self.error, 'url': self.url, 'id': self.id}
        )

    def store_image(self, db):
        db.exec(
            "UPDATE news SET image = :image WHERE id = :id",
            {'image': self.image.basename, 'id': self.id}
        )

    def store_redirect(self, db):
        db.exec(
            "UPDATE news SET redirect = link WHERE id = :id",
            {'id': self.id}
        )

    def store_league(self, db):
        db.exec(
            "UPDATE news SET id_league = :id_league WHERE id = :id",
            {'id_league': self.league.id, 'id': self.id}
        )

    def remove(self, db):
        db.exec(
            "DELETE FROM news WHERE id = :id",
            {'id': self.id}
        )

    @staticmethod
    def get_older_news(date_limit, db):
        for row in db.query("SELECT id, link, title, image FROM news WHERE pub_date < :date", {'date': date_limit}):
            news = News()
            news.id = row['id']
            news.url = row['link']
            news.title = row['title']
            news.image = NewsImage(basename=row['image'], id_news=news.id, title=news.title, url=news.url)
            yield news

    @staticmethod
    def update_news_counters(db):
        db.exec("""
            UPDATE leagues 
            SET leagues.nb_news = (SELECT COUNT(*) FROM news WHERE news.id_league = leagues.id)
        """)
        db.exec("""
            UPDATE teams
            SET teams.nb_news = (SELECT COUNT(*) FROM news WHERE news.id_team = team.id)
        """)
