class Month:
    def __init__(self, idt=None, name=None, current=None, bets=None, rank=None, end=None):
        self.id = idt if isinstance(idt, int) and 1 <= idt <= 12 else 0
        self.name = name
        self.current = current
        self.bets = bets
        self.rank = rank
        self.end = end

    def toggle_current(self, db):
        db.exec("UPDATE months SET current = 1 - current WHERE id = :id", {'id': self.id})

    def close_bets(self, db):
        db.exec("UPDATE bets SET open = 0 WHERE id_month = :id", {'id': self.id})

    @staticmethod
    def current_year(db):
        for row in db.query("SELECT year FROM years WHERE current = 1"):
            return row['year']

    @staticmethod
    def get_months(db):
        for row in db.query("SELECT * FROM months ORDER BY orderer ASC"):
            yield Month(
                idt=row['id'],
                name=row['name'],
                current=bool(row['current']),
                bets=bool(row['bets']),
                rank=bool(row['rank']),
                end=row['end']
            )
