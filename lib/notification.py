class Notification:

    def __init__(self, id_user, id_match, date, mday_or_round, type_, method):
        self.id = 0
        self.id_user = id_user
        self.id_match = id_match
        self.date = date
        self.mday_or_round = mday_or_round
        self.type = type_
        self.method = method
        self.sent = 0

    def save(self, db):
        res = None
        stmt = """
        INSERT INTO notifications (id_user, id_match, date, mday_or_round, type, method)
        VALUES (:id_user, :id_match, :date, :mday_or_round, :type, :method)
        """
        args = {
            'id_user': self.id_user, 'id_match': self.id_match, 'date': self.date.strftime('%Y-%m-%d %H:%M:%S'),
            'mday_or_round': self.mday_or_round, 'type': self.type, 'method': self.method
        }
        try:
            db.exec(stmt, args)
        except db.exceptions.IntegrityError:
            res = False
        else:
            res = True
        finally:
            return res
