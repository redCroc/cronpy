from datetime import datetime, timedelta
import subprocess
import os.path
import shlex
import time
import sys

from core.log import get_logger


def main():
    filename = sys.argv[1]
    if filename == 'update_scores':
        last_date = datetime.now() - timedelta(seconds=30)
        last_date = last_date.replace(second=0)
        frequency = timedelta(seconds=30)
    elif filename == 'update_news':
        last_date = datetime.now() - timedelta(minutes=1)
        last_date = last_date.replace(second=0)
        frequency = timedelta(minutes=1)
    elif filename == 'update_schedule':
        last_date = datetime.now() - timedelta(days=1)
        last_date = last_date.replace(hour=0)
        frequency = timedelta(days=1)
    elif filename == 'update_images':
        last_date = datetime.now() - timedelta(days=1)
        last_date = last_date.replace(hour=1)
        frequency = timedelta(days=1)
    elif filename == 'update_rankings':
        last_date = datetime.now() - timedelta(days=1)
        last_date = last_date.replace(hour=2)
        frequency = timedelta(days=1)
    elif filename == 'update_staff':
        last_date = datetime.now() - timedelta(days=1)
        last_date = last_date.replace(hour=3)
        frequency = timedelta(days=1)
    elif filename == 'update_admin_bets':
        last_date = datetime.now() - timedelta(days=1)
        last_date = last_date.replace(hour=4)
        frequency = timedelta(days=1)
    elif filename == 'update_notifications':
        last_date = datetime.now() - timedelta(days=1)
        last_date = last_date.replace(hour=5)
        frequency = timedelta(days=1)
    elif filename == 'update_preprod':
        last_date = datetime.now() - timedelta(days=1)
        last_date = last_date.replace(hour=6)
        frequency = timedelta(days=1)
    elif filename == 'update_month':
        last_date = datetime.now() - timedelta(days=1)
        last_date = last_date.replace(hour=7)
        frequency = timedelta(days=1)
    elif filename == 'update_tvschedule':
        last_date = datetime.now() - timedelta(days=1)
        last_date = last_date.replace(hour=8)
        frequency = timedelta(days=1)
    elif filename == 'create_schedule':
        last_date = datetime.now() - timedelta(days=1)
        last_date = last_date.replace(hour=22)
        frequency = timedelta(hours=12)
    else:
        raise Exception('File {}.py not found'.format(filename))

    logger = get_logger(name=filename)
    dirname = os.path.dirname(os.path.abspath(__file__))
    binary = os.path.join(dirname, 'venv/bin/python')
    prog = os.path.join(dirname, filename)
    args = shlex.split('{} {}.py'.format(binary, prog))

    while True:
        current_date = datetime.now()
        if current_date >= last_date + frequency:
            logger.info('[O] Launching module {}.py ...'.format(filename))
            last_date = current_date
            subprocess.call(args)
        time.sleep(1)


if __name__ == '__main__':
    main()
