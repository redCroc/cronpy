from traceback import format_exc
import os.path

from core.webutils import WebUtils
from lib.league import League
from lib.team import Team
import setting


class UpdateImages(WebUtils):

    TEAM_SIZES = {'30', '50', '80'}
    LEAGUE_SIZES = {'35', '50', '80'}

    def __init__(self):
        super().__init__(module_='update_images')
        teams = list(Team.get_teams(db=self.mysqldb, origin=self.module, id_team=self.args.id_team))
        leagues = list(League.get_leagues(db=self.mysqldb, origin=self.module, id_league=self.args.id_league))
        self.nb_tasks = len(teams) + len(leagues)
        self.start()
        self.update_team_images(teams)
        self.update_league_images(leagues)
        self.end()

    def update_team_images(self, teams):
        self.logger.info('[*] Update team images')
        for team in teams:
            try:
                images = dict()

                defpath = '{}/team/default-team.png'.format(setting.IMAGES_FOLDER)
                images['png'] = 'default-team.png?v={}'.format(int(os.path.getmtime(defpath)))
                path = '{}/team/{}.png'.format(setting.IMAGES_FOLDER, team.id)
                if os.path.exists(path):
                    images['png'] = '{}.png?v={}'.format(team.id, int(os.path.getmtime(path)))

                for size in self.TEAM_SIZES:
                    defpath = '{}/team/h{}-default-team.svg'.format(setting.IMAGES_FOLDER, size)
                    images[size] = 'h{}-default-team.svg?v={}'.format(size, int(os.path.getmtime(defpath)))
                    path = '{}/team/h{}-{}.svg'.format(setting.IMAGES_FOLDER, size, team.id)
                    if os.path.exists(path):
                        images[size] = 'h{}-{}.svg?v={}'.format(size, team.id, int(os.path.getmtime(path)))
                    elif team.id_sport == 2:
                        path = '{}/country/h{}-{}.svg'.format(setting.IMAGES_FOLDER, size, team.country.id)
                        if os.path.exists(path):
                            images[size] = '../country/h{}-{}.svg?v={}'.format(
                                size, team.country.id, int(os.path.getmtime(path))
                            )
                if images != team.images:
                    team.images = images
                    team.store_images(db=self.mysqldb)

            except BaseException as e:
                team.error = format_exc()
                self.logger.error('[-] team {}: {} - {}\n{}'.format(team.id, type(e), e, team.error))
                self.errors += 1
            else:
                team.error = None
                self.logger.info('[+] team {}: OK'.format(team.id))
                self.quantity += 1
                self.tasks_done += 1
            finally:
                team.store_error(db=self.mysqldb)

    def update_league_images(self, leagues):
        self.logger.info('[*] Update league images')
        for league in leagues:
            try:
                images = dict()

                defpath = '{}/league/default-league.png'.format(setting.IMAGES_FOLDER)
                images['png'] = 'default-league.png?v={}'.format(int(os.path.getmtime(defpath)))
                path = '{}/league/{}.png'.format(setting.IMAGES_FOLDER, league.id)
                if os.path.exists(path):
                    images['png'] = '{}.png?v={}'.format(league.id, int(os.path.getmtime(path)))

                for size in self.LEAGUE_SIZES:
                    defpath = '{}/league/h{}-default-league.svg'.format(setting.IMAGES_FOLDER, size)
                    images[size] = 'h{}-default-league.svg?v={}'.format(size, int(os.path.getmtime(defpath)))
                    path = '{}/league/h{}-{}.svg'.format(setting.IMAGES_FOLDER, size, league.id)
                    if os.path.exists(path):
                        images[size] = 'h{}-{}.svg?v={}'.format(size, league.id, int(os.path.getmtime(path)))

                if images != league.images:
                    league.images = images
                    league.store_images(db=self.mysqldb)

            except BaseException as e:
                league.error = format_exc()
                self.logger.error('[-] league {}: {} - {}\n{}'.format(league.id, type(e), e, league.error))
                self.erros += 1
            else:
                league.error = None
                self.logger.info('[+] league {}: OK'.format(league.id))
                self.quantity += 1
                self.tasks_done += 1
            finally:
                league.store_error(db=self.mysqldb)


if __name__ == '__main__':
    UpdateImages()
