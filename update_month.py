from collections import OrderedDict
from datetime import date

from lib.tools import n2v, real_round
from core.webutils import WebUtils
from lib.user import User
from lib.month import Month


class UpdateMonth(WebUtils):

    def __init__(self):
        super().__init__(module_='update_month')
        current_month, next_month = self.get_current_months()
        if current_month.end <= date.today():
            users = list(User.get_users_results(month=current_month, db=self.mysqldb))
            self.nb_tasks = len(users)
            self.start()
            self.save_users_results(month=current_month, users=users)
            current_month.toggle_current(db=self.mysqldb)
            current_month.close_bets(db=self.mysqldb)
            next_month.toggle_current(db=self.mysqldb)
            self.end()
        else:
            self.logger.info('[X] Cron unnecessary before {}'.format(current_month.end.strftime('%Y-%m-%d')))

    def save_users_results(self, month, users):
        year = Month.current_year(self.mysqldb)
        for user in users:
            self.logger.info('[+] user {}: {}'.format(user.id, user.name))
            self.quantity += 1
            record = {
                'id_league': 0,
                'id_team': 0,
                'league_rank': 0,
                'league_nbusers': 0,
                'league_points': 0,
                'europe': user.europe,
                'europe_round': n2v(user.europe_round)
            }
            if user.id_league > 0 and user.league_points > 0:
                record['id_league'] = user.id_league
                record['id_team'] = user.id_team
                record['league_rank'] = user.league_rank
                record['league_points'] = user.league_points
                record['league_nbusers'] = len(
                    [pl for pl in users if pl.id_league == user.id_league and pl.league_points > 0]
                )
            user.records.setdefault(str(year), dict())
            user.records[str(year)][str(month.id)] = record
            user.store_records(db=self.mysqldb)
            self.logger.debug('record: {}'.format(record))

            if month.rank:
                user.bob_details = OrderedDict()
                user.bob_points = 0
                _id_month = month.id
                _year = year
                for _ in range(10):
                    league_points = 0
                    europe_points = 0
                    if str(_year) in user.records and str(_id_month) in user.records[str(_year)]:
                        nb_users = user.records[str(_year)][str(_id_month)]['league_nbusers']
                        rank = user.records[str(_year)][str(_id_month)]['league_rank']
                        league_points = real_round(float(100 * nb_users) / (2 ** (rank - 1)))
                        europe = user.records[str(_year)][str(_id_month)]['europe']
                        europe_round = user.records[str(_year)][str(_id_month)]['europe_round']
                        europe_points_dict = {
                            91: {'winner': 1000, 'final': 500, 'semi': 250, 'quarter': 125},
                            92: {'winner': 600, 'final': 300, 'semi': 150, 'quarter': 75}
                        }
                        europe_points = europe_points_dict[europe][europe_round] if europe in europe_points_dict else 0

                    user.bob_details[str(_id_month)] = {
                        'league': league_points,
                        'europe': europe_points
                    }
                    user.bob_points += league_points
                    user.bob_points += europe_points

                    if _id_month == 1:
                        _id_month = 12
                    elif _id_month == 8:
                        _id_month = 5
                        _year -= 1
                    else:
                        _id_month -= 1
                self.logger.debug('bob_details: {}'.format(user.bob_details))
                self.logger.debug('bob_points: {}'.format(user.bob_points))
            self.tasks_done += 1

        if month.rank:
            self.logger.info('[*] BOB Ranking')
            users.sort(key=lambda x: x.bob_points, reverse=True)
            rank = 0
            for user in users:
                rank += 1
                user.bob_rank = rank
                self.logger.debug('{}. {} - {}'.format(user.bob_rank, user.name, user.bob_points))
                user.store_bob_rank(db=self.mysqldb)

    def get_current_months(self):
        current_month = None
        next_month = None
        done = False

        months = list(Month.get_months(db=self.mysqldb))
        for month in months:
            if done and month.bets:
                next_month = month
                break
            elif month.current:
                current_month = month
                done = True

        if current_month is None:
            raise NameError('No current month found !')
        if next_month is None:
            for month in months:
                if month.bets:
                    next_month = month
                    break
        if next_month is None:
            raise NameError('No next month found !')

        return current_month, next_month


if __name__ == '__main__':
    UpdateMonth()
