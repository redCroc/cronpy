from providers.base import BaseProvider

from bs4 import BeautifulSoup

from lib.tvchannel import TvChannel


class ProgrammeTelevision(BaseProvider):

    DOMAINS = {'www.programme-television.org'}
    CHARSET = 'utf-8'

    @classmethod
    def get_tvchannels(cls, source, data):
        html = data.decode(cls.CHARSET)
        soup = BeautifulSoup(html, 'html.parser')
        for li in soup.find_all('li', class_='col-md-2'):
            channel_name = li.find('span', class_='titre').text
            source.tv_channels.append(TvChannel(id_country=source.id_country, name=channel_name))
