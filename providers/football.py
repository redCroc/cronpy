from bs4 import BeautifulSoup

from providers.base import BaseProvider


class Football(BaseProvider):

    DOMAINS = {'www.football.fr'}
    CHARSET = 'utf-8'

    @classmethod
    def get_team_staff(cls, data):
        html = data.decode(cls.CHARSET)
        soup = BeautifulSoup(html, 'html.parser')
        table = soup.find('table', class_='effectif')
        players = list()
        staff = dict()
        for tr in table.find_all('tr'):
            th = tr.find('th')
            if th is not None:
                if th.text.strip() == 'Défenseur':
                    staff['goalkeepers'] = players
                    players = list()
                elif th.text.strip() == 'Milieu':
                    staff['defenders'] = players
                    players = list()
                elif th.text.strip() == 'Attaquant':
                    staff['midfielders'] = players
                    players = list()
            else:
                a = tr.find('td', class_='player left').find('a')
                players.append(a.text.strip())
        staff['attackers'] = players
        return staff
