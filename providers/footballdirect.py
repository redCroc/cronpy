from bs4 import BeautifulSoup

from providers.base import BaseProvider


class FootballDirect(BaseProvider):

    DOMAINS = {'www.football-direct.com'}
    CHARSET = 'iso-8859-15'

    @classmethod
    def get_team_staff(cls, data):
        html = data.decode(cls.CHARSET)
        soup = BeautifulSoup(html, 'html.parser')
        sections = soup.find('div', id='tabpanel2').find_all('section')
        players = list()
        staff = dict()
        for section in sections:
            h3 = section.find('h3')
            if h3.text.strip() == 'Défenseurs':
                staff['goalkeepers'] = players
                players = list()
            elif h3.text.strip() == 'Milieu de terrain':
                staff['defenders'] = players
                players = list()
            elif h3.text.strip() == 'Attaquants':
                staff['midfielders'] = players
                players = list()
            for div in section.find_all('div', class_='block6-1'):
                players.append(div.find('div', class_='title4_1').find('strong').text.strip())
        staff['attackers'] = players
        return staff
