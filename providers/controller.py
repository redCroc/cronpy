from urllib.parse import urlsplit

from providers.programmetelevision import ProgrammeTelevision
from providers.footballdirect import FootballDirect
from providers.matchendirect import Matchendirect
from providers.transfermarkt import TransferMarkt
from providers.footmercato import FootMercato
from providers.football365 import Football365
from providers.eurosport import Eurosport
from providers.football import Football
from providers.lequipe import Lequipe
from providers.footao import Footao
from providers.fftt import Fftt


class ProviderController:

    PROVIDERS = [
        ProgrammeTelevision,
        FootballDirect,
        Matchendirect,
        TransferMarkt,
        FootMercato,
        Football365,
        Eurosport,
        Football,
        Lequipe,
        Footao,
        Fftt
    ]

    @classmethod
    def get_provider(cls, url):
        netloc = urlsplit(url).netloc
        for provider in cls.PROVIDERS:
            if netloc in provider.DOMAINS:
                return provider
