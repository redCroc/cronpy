from bs4 import BeautifulSoup

from providers.base import BaseProvider


class Footao(BaseProvider):

    DOMAINS = {'www.footao.tv'}
    CHARSET = 'utf-8'

    @classmethod
    def get_tvschedule(cls, league, tv_channels, data):
        html = data.decode(cls.CHARSET)
        soup = BeautifulSoup(html, 'html.parser')
        for div in soup.find_all('div'):
            a = div.find('a', class_='rc')
            if a is not None and ' · ' in a.text:
                home, away = a.text.split(' · ')
                for match in league.matches:
                    if match.home.names.get('footao') == home and match.away.names.get('footao') == away:
                        for img in div.find_all('img'):
                            tvc_name = img['alt'].replace('programme foot', '').split('tv direct')[0].strip()
                            for tv_channel in tv_channels:
                                if tv_channel.names.get('footao') == tvc_name:
                                    match.tv_channels.append(tv_channel)
                        break
