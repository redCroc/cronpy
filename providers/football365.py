from bs4 import BeautifulSoup

from providers.base import BaseProvider


class Football365(BaseProvider):

    DOMAINS = {'www.football365.fr'}
    CHARSET = 'utf-8'

    @classmethod
    def get_team_staff(cls, data):
        html = data.decode(cls.CHARSET)
        soup = BeautifulSoup(html, 'html.parser')
        table = soup.find('table', class_='table table-striped')
        players = list()
        staff = dict()
        for td in table.find_all('td'):
            if 'players-effectif' in td.attrs['class']:
                h3 = td.find('h3')
                if h3.text.strip() == 'Défenseurs':
                    staff['goalkeepers'] = players
                    players = list()
                elif h3.text.strip() == 'Milieux':
                    staff['defenders'] = players
                    players = list()
                elif h3.text.strip() == 'Attaquants':
                    staff['midfielders'] = players
                    players = list()
            elif 'nom_joueur' in td.attrs['class']:
                a = td.find('a')
                if len(a.contents) > 1:
                    player = '{} {}'.format(a.contents[0].strip(), a.contents[1].text.strip())
                else:
                    player = a.contents[0].strip()
                players.append(player)
        staff['attackers'] = players
        return staff
