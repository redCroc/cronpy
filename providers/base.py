class BaseProvider:

    @classmethod
    def get_match_info(cls, match, data):
        pass

    @classmethod
    def get_team_staff(cls, data):
        pass

    @classmethod
    def get_team_players(cls, data, team, countries):
        pass

    @classmethod
    def get_league_ranking(cls, league, data):
        pass

    @classmethod
    def get_group_ranking(cls, group, data):
        pass

    @classmethod
    def get_newss_from_source(cls, news_source, data):
        pass

    @classmethod
    def get_news_content(cls, news, data):
        pass

    @classmethod
    def get_news_image(cls, news_image, data):
        pass

    @classmethod
    def get_schedule_url(cls, match):
        pass

    @classmethod
    def get_schedule(cls, scheduler, data):
        pass

    @classmethod
    def get_tvchannels(cls, source, data):
        pass

    @classmethod
    def get_tvschedule(cls, league, tv_channels, data):
        pass

    @classmethod
    def create_schedule(cls, league, data):
        pass

    @classmethod
    def create_schedule_from_url(cls, league, data):
        pass
