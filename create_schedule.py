from traceback import format_exc
from copy import copy

from providers.controller import ProviderController
from core.webutils import WebUtils
from lib.league import League
from lib.team import Team


class CreateSchedule(WebUtils):
    def __init__(self):
        super().__init__(module_='create_schedule')
        leagues = list(League.get_leagues(db=self.mysqldb, origin=self.module, id_league=self.args.id_league))
        self.nb_tasks = len(leagues)
        self.start()
        self.run(leagues, self.create_schedule)
        self.end()

    def create_schedule(self, league, data):
        try:
            provider = ProviderController.get_provider(league.url)

            if provider.__name__ == 'Eurosport':
                old_round = None
                for match in provider.create_schedule(league, data):
                    match.home.country.build_from_name(db=self.mysqldb)
                    match.home.images = {
                        str(size):
                            '../country/h{}-{}.svg'.format(size, match.home.country.id)
                            if match.home.country.id is not None 
                            else 'h{}-default-team.svg'.format(size)
                        for size in (30, 50, 80)
                    }
                    match.home.store(db=self.mysqldb)

                    match.away.country.build_from_name(db=self.mysqldb)
                    match.away.images = {
                        str(size):
                            '../country/h{}-{}.svg'.format(size, match.away.country.id)
                            if match.away.country.id is not None
                            else 'h{}-default-team.svg'.format(size)
                        for size in (30, 50, 80)
                    }
                    match.away.store(db=self.mysqldb)

                    if match.round != old_round:
                        match.league.store_round(new_round=match.round, db=self.mysqldb)
                        old_round = match.round

                    match.store(db=self.mysqldb)
                    if match.id > 0:
                        self.logger.info('[+] match {}: {} vs {}: OK'.format(match.id, match.home.id, match.away.id))
                        self.quantity += 1

            elif provider.__name__ == 'Matchendirect':
                leagues = list()
                for url in provider.create_schedule(league, data):
                    league_copy = copy(league)
                    league_copy.url = url
                    leagues.append(league_copy)
                self.run(leagues, self.create_schedule_from_url)

        except BaseException as e:
            league.error = format_exc()
            self.logger.error('[-] league {}: {} - {}\n{}'.format(league.id, type(e), e, league.error))
            self.errors += 1
        else:
            league.error = None
            self.logger.info('[+] league {}: OK'.format(league.id))
            self.tasks_done += 1
        finally:
            league.store_error(self.mysqldb)

    def create_schedule_from_url(self, league, data):
        try:
            provider = ProviderController.get_provider(league.url)
            for match in provider.create_schedule_from_url(league, data):
                _home = Team.from_source(names=match.home.names, league=league, db=self.mysqldb)
                if _home is not None:
                    match.home = _home
                else:
                    match.home.store(self.mysqldb)

                _away = Team.from_source(names=match.away.names, league=league, db=self.mysqldb)
                if _away is not None:
                    match.away = _away
                else:
                    match.away.store(self.mysqldb)

                match.store(self.mysqldb)
                if match.id > 0:
                    self.logger.info('[+] match {}: {} vs {}: OK'.format(match.id, match.home.id, match.away.id))
                    self.quantity += 1

        except BaseException as e:
            league.error = format_exc()
            self.logger.error('[-] league {}={}: {} - {}\n{}'.format(league.id, league.url, type(e), e, league.error))
        else:
            league.error = None
            self.logger.info('[+] league {}={}: OK'.format(league.id, league.url))
        finally:
            league.store_error(self.mysqldb)


if __name__ == '__main__':
    CreateSchedule()
