from traceback import format_exc
import random

from core.webutils import WebUtils
from lib.user import User


class UpdateAdminBets(WebUtils):

    def __init__(self):
        super().__init__(module_='update_admin_bets')
        league_users = list(User.get_users_league_bets(db=self.mysqldb, admin=True))
        europe_users = list(User.get_users_europe_bets(db=self.mysqldb, admin=True))
        self.nb_tasks = len(league_users) + len(europe_users)
        self.start()
        self.update_admin_league(users=league_users)
        self.update_admin_europe(users=europe_users)
        self.end()

    def update_admin_league(self, users):
        self.logger.info('[*] Update users league bets')
        for user in users:
            try:
                self.logger.debug('user {} ({}): start'.format(user.id, user.name))
                id_month = 0
                for mday, matches in user.league_bets_matches.items():
                    for match in matches:
                        id_month = match.id_month
                        coeff_home = match.home.coeff
                        coeff_away = match.away.coeff
                        coeff_tie = (coeff_home + coeff_away) // 3
                        rd_choice = random.choice(range(coeff_home + coeff_away + coeff_tie))
                        if rd_choice < coeff_home:
                            bet = '1'
                        elif rd_choice < coeff_home + coeff_away:
                            bet = '2'
                        else:
                            bet = 'X'
                        user.league_bets[mday][match.idof10] = bet
                        self.logger.debug('match {} - bet {}'.format(match.id, bet))
                user.set_league_bets(id_month=id_month, db=self.mysqldb)

            except BaseException as e:
                self.logger.error('[-] user {} ({}): {} - {}\n{}'.format(user.id, user.name, type(e), e, format_exc()))
                self.errors += 1
            else:
                self.logger.info('[+] user {} ({}): OK'.format(user.id, user.name))
                self.tasks_done += 1
                self.quantity += 1

    def update_admin_europe(self, users):
        self.logger.info('[*] Update users europe bets')
        for user in users:
            try:
                self.logger.debug('user {} ({}): start'.format(user.id, user.name))
                id_month = 0
                for match in user.europe_bets_matches:
                    if match is not None:
                        id_month = match.id_month
                        score_home = random.choice((0, 1, 1, 2, 2, 3))
                        score_away = random.choice((0, 0, 1, 1, 2, 3))
                        user.europe_scores[match.idof4]['home'] = score_home
                        user.europe_scores[match.idof4]['away'] = score_away
                        user.europe_scorers[match.idof4]['home'] = random.choices(
                            match.home.staff['attackers'], k=score_home
                        )
                        user.europe_scorers[match.idof4]['away'] = random.choices(
                            match.away.staff['attackers'], k=score_away
                        )
                        self.logger.debug('match {} - bet {}:{}'.format(match.id, score_home, score_away))
                user.set_europe_bets(id_month=id_month, db=self.mysqldb)

            except BaseException as e:
                self.logger.error('[-] user {} ({}): {} - {}\n{}'.format(user.id, user.name, type(e), e, format_exc()))
                self.errors += 1
            else:
                self.logger.info('[+] user {} ({}): OK'.format(user.id, user.name))
                self.tasks_done += 1
                self.quantity += 1


if __name__ == '__main__':
    UpdateAdminBets()
